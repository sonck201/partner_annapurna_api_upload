#!/bin/bash

rm -rf /var/www/html/logs/*
chmod -R 777 /var/www/html/logs
chown -R nginx:nginx /var/www/html

exec $@
